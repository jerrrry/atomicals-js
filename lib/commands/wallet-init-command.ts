import { CommandResultInterface } from "./command-result.interface";
import { CommandInterface } from "./command.interface";
import { createKeyPair, createPrimaryAndFundingKeyPairs } from "../utils/create-key-pair";
import { jsonFileExists, jsonFileWriter } from "../utils/file-utils";
import { walletPathResolver } from "../utils/wallet-path-resolver";
import { createMnemonicPhrase } from "../utils/create-mnemonic-phrase";

const walletPath = walletPathResolver();

export class WalletInitCommand implements CommandInterface {
  constructor(private phrase: string | undefined, private path: string) {}
  async run(): Promise<CommandResultInterface> {
    if (await this.walletExists()) {
      throw "wallet.json exists, please remove it first to initialize another wallet. You may also use 'wallet-create' command to generate a new wallet.";
    }

    const wallet = await createPrimaryAndFundingKeyPairs(
      this.phrase,
      this.path
    );

    const imported_number = 1000; // 预设多少个imported数量
    const walletJson = {
      phrase: wallet.phrase,
      primary: {
        address: wallet.primary.address,
        path: wallet.primary.path,
        WIF: wallet.primary.WIF,
      },
      funding: {
        address: wallet.funding.address,
        path: wallet.funding.path,
        WIF: wallet.funding.WIF,
      },
      imported: {},
    };
    
    let phraseResult: any = this.phrase;
    if (!phraseResult) {
        phraseResult = await createMnemonicPhrase();
        phraseResult = phraseResult.phrase;
    }
    for (let i = 0; i < imported_number; i++) {
      const imported = await createKeyPair(
        phraseResult,
        `${this.path}/1/${i + 1}`
      );
      walletJson.imported[`funding${i + 1}`] = {
        address: imported.address,
        path: imported.path,
        WIF: imported.WIF,
      };
    }

    await jsonFileWriter(walletPath, walletJson);
    return {
      success: true,
      data: wallet,
    };
  }
  async walletExists() {
    if (await jsonFileExists(walletPath)) {
      return true;
    }
  }
}
