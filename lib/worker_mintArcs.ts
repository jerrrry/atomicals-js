import { Atomicals, ElectrumApi } from '.';
const workerpool = require('workerpool');
import * as fs from 'fs';

const atomicals = new Atomicals(
  ElectrumApi.createClient(process.env.ELECTRUMX_PROXY_BASE_URL || '')
);

const fn = (
  files,
  initialOwnerAddressAddress,
  fundingRecordWIF,
  {
    meta,
    ctx,
    init,
    satsbyte,
    satsoutput,
    container,
    bitworkc,
    bitworkr,
    parent,
    parentOwnerRecord,
    disablechalk,
  }
) => {
  return atomicals.mintNftInteractive(files, initialOwnerAddressAddress, fundingRecordWIF, {
    meta: meta,
    ctx: ctx,
    init: init,
    satsbyte: parseInt(satsbyte),
    satsoutput: parseInt(satsoutput),
    container: container,
    bitworkc: bitworkc,
    bitworkr: bitworkr,
    parent: parent,
    parentOwner: parentOwnerRecord,
    disableMiningChalk: disablechalk,
  });
};

workerpool.worker({ mintArcs: fn });
