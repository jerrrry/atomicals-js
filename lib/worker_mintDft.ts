import { Atomicals, ElectrumApi } from ".";
const workerpool = require("workerpool");

const atomicals = new Atomicals(
    ElectrumApi.createClient(process.env.ELECTRUMX_PROXY_BASE_URL || '')
);
const fn = (address: string, ticker: string, WIF: string, options: any) => {
    return atomicals.mintDftInteractive(
        address,
        ticker,
        WIF,
        options
    )
}

workerpool.worker({ mintDft: fn });