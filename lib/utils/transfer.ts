import { networks } from 'bitcoinjs-lib';
import { NETWORK } from '../commands/command-helpers';
import * as bip39 from 'bip39';
import { BIP32Factory } from 'bip32';
import * as ecc from 'tiny-secp256k1';
import * as bitcoin from 'bitcoinjs-lib';
import ECPairFactory from 'ecpair';
import axios from 'axios';
import { Atomicals, ElectrumApi } from '..';
import * as fs from 'fs';
require('dotenv').config();
bitcoin.initEccLib(ecc); // 生成p2tr地址必须调用这个方法initEccLib
const ECPair = ECPairFactory(ecc);

const SEND_RETRY_SLEEP_SECONDS = 15;
const SEND_RETRY_ATTEMPTS = 20;

export async function prToPrOneToMany(
  fromWIF: string,
  to: Record<string, number>,
  feeRate: number
) {
  let blockStream = 'https://blockstream.info';
  if (NETWORK === networks.testnet) {
    blockStream = 'https://blockstream.info/testnet';
  }
  const keyPair = ECPair.fromWIF(fromWIF);
  const { address: fromAddress, output } = bitcoin.payments.p2tr({
    internalPubkey: keyPair.publicKey.subarray(1, 33),
    network: NETWORK,
  });
  if (!fromAddress || !output) {
    throw new Error(`生成p2tr地址失败`);
  }
  // 通过WIF获取childNodeXOnlyPubkey
  const childNodeXOnlyPubkey = keyPair.publicKey.subarray(1, 33);
  const tweakedChildNode = keyPair.tweak(
    bitcoin.crypto.taggedHash('TapTweak', childNodeXOnlyPubkey)
  );

  // 获取utxos
  const utxoResponse = await axios.get(`${blockStream}/api/address/${fromAddress}/utxo`);
  if (utxoResponse.status !== 200) {
    throw new Error(`获取utxo失败`);
  }

  console.log('### utxo数量：', utxoResponse.data.length);
  console.log('### utxo:', utxoResponse.data);

  const utxos: [] = utxoResponse.data;

  // utxos总额
  const utxosAmount = utxos.reduce((prev: number, cur: any) => {
    return prev + cur.value;
  }, 0);

  // 添加UTXOs到交易

  let psbt = new bitcoin.Psbt({ network: NETWORK }); // 创建一个Psbt对象
  let inputUtxosAmount = 0; // 加入到input uxos的总额
  let fee = 0; // 手续费总额
  let estimateFee = 0; // 预估手续费
  const toAmount = Object.values(to).reduce((prev: number, cur: number) => {
    return prev + cur;
  }, 0); // 转账总额

  console.log('### utxo总额：', utxosAmount);
  console.log('### to总额', toAmount);
  let i: number = 1;

  do {
    console.log(
      `\n### 开始本次循环计算${i},inputUtxosAmount:${inputUtxosAmount},toAmount:${toAmount},fee:${fee},estimateFee:${estimateFee}`
    );
    fee = estimateFee;
    if (utxosAmount < toAmount + fee) {
      throw new Error(`utxos总额小于to+fee总额`);
    }
    psbt = new bitcoin.Psbt({ network: NETWORK }); // 创建一个Psbt对象
    psbt.setVersion(2); // These are defaults. This line is not needed.
    psbt.setLocktime(0); // These are defaults. This line is not needed.
    inputUtxosAmount = 0;

    utxos.some((utxo: any) => {
      inputUtxosAmount += utxo.value;
      psbt.addInput({
        // if hash is string, txid, if hash is Buffer, is reversed compared to txid
        hash: utxo.txid,
        index: utxo.vout,
        sequence: 0xffffffff, // These are defaults. This line is not needed.
        witnessUtxo: {
          value: utxo.value,
          script: output,
        },
        tapInternalKey: childNodeXOnlyPubkey,
      });
      console.log(
        `^^^ 加入inputUtxo:${utxo.txid}:${utxo.vout},value:${utxo.value},inputUtxosAmount:${inputUtxosAmount},toAmount:${toAmount},fee:${fee},estimateFee:${estimateFee}`
      );
      if (inputUtxosAmount >= toAmount + fee) {
        console.log('++++++++结束循环', inputUtxosAmount >= toAmount + fee);
        return true;
      }
    });

    for (const key in to) {
      if (to.hasOwnProperty(key)) {
        const value = to[key];
        psbt.addOutput({
          address: key,
          value: value,
        });
      }
    }
    // 找零 inputUtxosAmount-toAmount-fee
    const changeAmount = inputUtxosAmount - toAmount - fee;

    console.log('change', changeAmount);

    if (changeAmount > 43 * feeRate) {
      console.log(
        `### 需要找零:${changeAmount},inutUxtosAmount:${inputUtxosAmount},toAmount:${toAmount},fee:${fee},`
      );
      // 多加一个output需要43的大小
      const changeAddress = fromAddress;
      psbt.addOutput({
        address: changeAddress,
        value: changeAmount,
      });
    } else {
      console.log(
        `### 不找零了:${changeAmount},inutUxtosAmount:${inputUtxosAmount},toAmount:${toAmount},fee:${fee},`
      );
    }
    for (let i = 0; i < psbt.inputCount; i++) {
      psbt.signInput(i, tweakedChildNode);
    }
    psbt.finalizeAllInputs();
    const estimatedSize = psbt.extractTransaction().byteLength(); // 估算交易大小
    const estimatedVirtualSize = psbt.extractTransaction().virtualSize(); // 估算虚拟大小

    // 计算手续费
    estimateFee = feeRate * estimatedVirtualSize; // 重新计算预估手续费
    console.log('### inputs', psbt.txInputs);
    console.log(
      `### 估算交易大小：${estimatedSize},估算虚拟大小：${estimatedVirtualSize},新交易预估的手续费:${estimateFee}`
    );
    console.log(
      `### 结束本次循环计算${i},inputUtxosAmount:${inputUtxosAmount},toAmount:${toAmount},fee:${fee},estimateFee:${estimateFee}`
    );
    i++;
  } while (inputUtxosAmount < toAmount + fee || estimateFee !== fee);

  const rawtx = psbt.extractTransaction().toHex();

  fs.appendFileSync(
    'task.log',
    `资金转账准备广播，inputUtxosAmount:${inputUtxosAmount},toAmount:${toAmount},fee:${fee}}>>>>>>>>>>>>>\n`
  );

  const result: any = await broadcastWithRetries(rawtx);
  if (!result) {
    fs.appendFileSync(
      'task.log',
      `资金转账异常，inputUtxosAmount:${inputUtxosAmount},toAmount:${toAmount},fee:${fee} result:${result}}>>>>>>>>>>>>>\n`
    );
    throw new Error(`广播交易失败`);
  }

  fs.appendFileSync('task.log', `资金广播完成，result:${result}}>>>>>>>>>>>>>\n`);
}

async function broadcastWithRetries(rawtx: string) {
  let attempts = 0;
  let result: any = null;

  const atomicals = new Atomicals(
    ElectrumApi.createClient(process.env.ELECTRUMX_PROXY_BASE_URL || '')
  );
  do {
    try {
      console.log('rawtx', rawtx);

      result = await atomicals.broadcast(rawtx);
      if (result) {
        break;
      }
    } catch (err) {
      console.log('Network error broadcasting (Trying again soon...)', err);
      // Put in a sleep to help the connection reset more gracefully in case there is some delay
      console.log(
        `Will retry to broadcast transaction again in ${SEND_RETRY_SLEEP_SECONDS} seconds...`
      );
    }
    attempts++;
  } while (attempts < SEND_RETRY_ATTEMPTS);
  return result;
}
